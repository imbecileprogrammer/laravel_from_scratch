@extends('layouts.app')

@section('content')
  <h1> All Categories</h1>

  <div class="col-md-10">


        @if(Session::has('flash_message'))
          <div class="alert alert-success">
            {{Session::get('flash_message')}}
          </div>
        @endif

{{----------------- SEARCH FORM -----------------}}

    <div class="pull-right">
      {!! Form::open(['method'=>'GET','url'=>'categories','class'=>'navbar-form navbar-left','role'=>'search'])  !!}
      <div class="input-group custom-search-form">
          <input type="text" class="form-control" name="search" placeholder="Search...">
          <span class="input-group-btn">
              <button class="btn btn-default-sm" type="submit">
                  <i class="fa fa-search"><!--<span class="hiddenGrammarError" pre="" data-mce-bogus="1"--></i>
              </button>
          </span>
      </div>
      {!! Form::close() !!}
    </div>


{{----------------- END SEARCH FORM -----------------}}




    <table class='table table-striped table-bordered table-hover table-condensed'>
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($categories as $category)
          <tr>
            <td>{{$category->name}}</td>
            <td>{{$category->description}}</td>
            <td>
              <div class="col-md-3">
                <a class="btn btn-primary" href="{{route('categories.edit',$category->id)}}">Edit</a>
              </div>
              <div class="col-md-2">
                {!! Form::open(['method' => 'DELETE', 'route' => ['categories.destroy',$category->id], 'class' => 'form-horizontal']) !!}
                    {!! Form::submit("Delete", ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
              </div>

            </td>
          </tr>
        @endforeach

      </tbody>
    </table>

    {{$categories->render()}}

    <div class="pull-right">
        <a href="categories/create" class="btn btn-default">Create Category</a>
    </div>

  </div>

@stop
