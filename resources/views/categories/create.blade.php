@extends('layouts.app')

@section('content')
  <h1> Create Category <a class="btn-sm btn-primary" href="/categories"> Back to all categories</a></h1>

{!! Form::open(['method' => 'POST', 'route' => 'categories.store', 'class' => 'form-horizontal']) !!}

        <div class="col-md-8">

          <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              {!! Form::label('name', 'Category Name') !!}
              {!! Form::text('name', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('name') }}</small>
          </div>

          <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              {!! Form::label('description', 'Category Description') !!}
              {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('description') }}</small>
          </div>

          <br>

          <div class="pull-right">
            {!! Form::reset("Reset", ['class' => 'btn btn-warning']) !!}
            {!! Form::submit("Add", ['class' => 'btn btn-success']) !!}
          </div>

        </div>



{!! Form::close() !!}
@stop
