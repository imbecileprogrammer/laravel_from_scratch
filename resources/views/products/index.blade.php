@extends('layouts.app')

@section('content')
  <h1>All Products</h1>
  <div class="col-md-10">


    @if(Session::has('flash_message'))
      <div class="alert alert-success">
        {{Session::get('flash_message')}}
      </div>
    @endif


    {{----------------- SEARCH FORM -----------------}}

        <div class="pull-right">
          {!! Form::open(['method'=>'GET','url'=>'products','class'=>'navbar-form navbar-left','role'=>'search'])  !!}
          <div class="input-group custom-search-form">
              <input type="text" class="form-control" name="search" placeholder="Search...">
              <span class="input-group-btn">
                  <button class="btn btn-default-sm" type="submit">
                      <i class="fa fa-search"><!--<span class="hiddenGrammarError" pre="" data-mce-bogus="1"--></i>
                  </button>
              </span>
          </div>
          {!! Form::close() !!}
        </div>


    {{----------------- END SEARCH FORM -----------------}}




    <table class='table table-striped table-bordered table-hover table-condensed'>
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Category</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        @foreach($products as $product)
          <tr>
            <td>{{$product->name}}</td>
            <td>{{$product->description}}</td>
            <td>{{$product->category->name}}</td>
            <td>
              <div class="col-md-3">
                  <a class="btn btn-primary" href="{{route('products.edit',$product->id)}}"> Edit</a>
              </div>
              <div class="col-md-2">
                {!! Form::open(['method' => 'DELETE', 'route' => ['products.destroy',$product->id], 'class' => 'form-horizontal']) !!}
                      {!! Form::submit("Delete", ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
              </div>

            </td>

          </tr>
        @endforeach

      </tbody>
    </table>
    <div class="pull-right">
      <a href="/products/create" class="btn btn-default">Create Product</a>
    </div>

  </div>
@endsection
