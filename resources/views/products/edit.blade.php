@extends('layouts.app')

@section('content')
  <h1>Edit Product  <a href="/products" class="btn-sm btn-primary">Back to All Products</a></h1>

  <div class="col-md-8">
    {!! Form::model($product, ['route' => ['products.update', $product->id], 'method' => 'PUT' ,'files' => true] ) !!}
        @if($product->image!=null)
            <img src="data:image;base64,{{ base64_encode($product->image) }}" alt="" />
        @endif

        <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
            {!! Form::label('image', 'Product Image') !!}
            {!! Form::file('image') !!}
            <p class="help-block">Image Here</p>
            <small class="text-danger">{{ $errors->first('image') }}</small>
        </div>

        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
            {!! Form::label('name', 'Product Name') !!}
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('name') }}</small>
        </div>

        <div class="form-group{{ $errors->has('category_id') ? ' has-error' : '' }}">
            {!! Form::label('category_id', 'Category') !!}
            {!! Form::select('category_id', $categories,null, ['class' => 'form-control']) !!}
            <small class="text-danger">{{ $errors->first('category_id') }}</small>
        </div>


              <div class="form-group{{ $errors->has('price') ? ' has-error' : '' }}">
                  {!! Form::label('price', 'Product Price') !!}
                  {!! Form::number('price', null, ['class' => 'form-control','step' => 'any']) !!}
                  <small class="text-danger">{{ $errors->first('price') }}</small>
              </div>


          <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
              {!! Form::label('description', 'Product Description') !!}
              {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
              <small class="text-danger">{{ $errors->first('description') }}</small>
          </div>

        <br>


        <div class="btn-group pull-right">
            {!! Form::submit("Update", ['class' => 'btn btn-success']) !!}
        </div>

    {!! Form::close() !!}

  </div>

@endsection
