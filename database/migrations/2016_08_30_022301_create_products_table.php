<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('products', function (Blueprint $table) {

        // https://laravel.com/docs/4.2/schema

        $table->increments('id');
        $table->integer('category_id')->unsigned();
        $table->string('name');
        $table->string('description');
        $table->double('price');
        $table->binary('image');

        $table->timestamps();

        // https://laravel.com/docs/5.3/eloquent-relationships#one-to-many --all about laravel relationship
        // https://laravel.com/docs/4.2/schema#foreign-keys

        $table->foreign('category_id')->references('id')->on('categories')
                            ->onDelete('cascade'); //on cascade delete  means that all the products connected to category will also be deleted when category is deleted.
    });


  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
      Schema::drop('products');
  }
}
