<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Product;
use App\Category;
use Session;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Response;
use Image;


class ProductsController extends Controller
{



  public function __construct(){
    $this->middleware(['auth','admin'],['except' => ['productlist']]);
  }

  public function productlist(){

    $categories = Category::all();

    return view('welcome',compact('categories'));

  }
  public function index(){

    $search = \Request::get('search'); //<-- we use global request to get the param of URI

    $products = Product::where('name','like','%'.$search.'%')
        ->orWhere('description','like','%'.$search.'%')
        ->orderBy('name')
        ->paginate(5);

      return view('products.index',compact('products'));
  }

  public function create(){

    $categories = Category::lists('name','id');
    return view('products.create',compact('categories'));
  }

  public function edit($id){

    $categories = Category::lists('name','id');
    $product = Product::findOrFail($id);

    return view('products.edit',compact('product','categories'));
  }


  public function store(Request $request){

    $input = $request->all();

    $this->validate($request,[
      'name' => 'required',
      'description' => 'required'
    ]);

    if($request['image'] !=  null){

       $img = Image::make($input['image']);
       $img->resize(320, 240);
       $img->response('jpeg');
       $input['image'] = $img;

    }

    Product::create($input);
    Session::flash('flash_message','Product Successfully Created');
    return redirect('/products');
  }


  public function update(Request $request,$id){

    $input = $request->all();

    $this->validate($request,[
      'name' => 'required',
      'description' => 'required'
    ]);

    if($request['image'] !=  null){

       $img = Image::make($request['image']);
       $img->resize(320, 240);
       Response::make($img->encode('jpeg'));
       $input['image'] = $img;
    }


      $product = Product::findOrFail($id);
      $product->fill($input)->save();
      Session::flash('flash_message','Product Successfully Updated');
      return redirect('/products');

  }


  public function destroy($id){
    $product = Product::findOrFail($id);
    $product->delete();
    Session::flash('flash_message','Product Successfully Deleted');
    return redirect('/products');
  }

}
