<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Category; // Category Model
use Session;

class CategoriesController extends Controller
{

  public function __construct(){
    $this->middleware(['auth','admin']);
  }

  public function index(){

    $search = \Request::get('search'); //<-- we use global request to get the param of URI

    $categories = Category::where('name','like','%'.$search.'%')
        ->orWhere('description','like','%'.$search.'%')
        ->orderBy('name')
        ->paginate(5);

    return view('categories.index',compact('categories'));
  }

  public function create(){



    return view('categories.create');
  }

  public function edit($id){

    $category = Category::findOrFail($id);
    return view('categories.edit',compact('category'));
  }


  public function store(Request $request){

    $this->validate($request,[
      'name' => 'required|unique:categories,name',
      'description' => 'required'
    ]);

    $input = $request->all();
    Category::create($input);
    Session::flash('flash_message','Category Successfully Created');
    return redirect('categories');

  }

  public function update($id,Request $request){

    $input = $request->all();
    $category = Category::findOrFail($id);

    if($category->name == $input['name']){

      $this->validate($request,[
        'name' => 'required',
        'description' => 'required'
      ]);

    }else{
      $this->validate($request,[
        'name' => 'required|unique:categories,name',
        'description' => 'required'
      ]);
    }


    $category->fill($input)->save();
    Session::flash('flash_message','Category Successfully Updated');
    return redirect('categories');


  }

  public function destroy($id){

    $category = Category::findOrFail($id);
    $category->delete();

    Session::flash('flash_message','Category Successfully Deleted');
    return redirect('categories');

  }
}
