<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::auth();

Route::get('/', 'ProductsController@productlist');

Route::get('/home', 'HomeController@index');

// --------------------- Categories Route -------------------------------

Route::get('/categories','CategoriesController@index');
Route::get('/categories/create','CategoriesController@create');

Route::get('/categories/edit/{id}',[
  'as' => 'categories.edit',
  'uses' => 'CategoriesController@edit'
]);

Route::post('/categories/store',[
  'as' => 'categories.store',
  'uses' => 'CategoriesController@store'
]);

Route::put('/categories/update/{id}',[
  'as' => 'categories.update',
  'uses' => 'CategoriesController@update'
]);


Route::delete('/categories/destroy/{id}',[
  'as' => 'categories.destroy',
  'uses' => 'CategoriesController@destroy'
]);


// ---------------------End  Categories Route ------------------------------




// --------------------- Products Route -------------------------------

Route::get('/products','ProductsController@index');
Route::get('/products/create','ProductsController@create');

Route::get('/products/edit/{id}',[
  'as' => 'products.edit',
  'uses' => 'ProductsController@edit'
]);

Route::post('/products/store',[
  'as' => 'products.store',
  'uses' => 'ProductsController@store'
]);


Route::put('/products/update/{id}',[
  'as' => 'products.update',
  'uses' => 'ProductsController@update'
]);

Route::delete('/products/destroy/{id}',[
  'as' => 'products.destroy',
  'uses' => 'ProductsController@destroy'
]);

// --------------------- End Products Route -------------------------------
