<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

  protected $table = "products";
  protected $primaryKey = "id";
  protected $fillable = [
    'name',
    'description',
    'category_id',
    'price',
    'image'
  ];

  public function category(){
    return $this->belongsTo('App\Category','category_id','id');
  }
}
